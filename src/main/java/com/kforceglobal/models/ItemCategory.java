package com.kforceglobal.models;

import javax.persistence.*;

@Entity
@Table(name = "item_category")
public class ItemCategory extends Identifiable {

	@Column(nullable = false, unique = true)
	private String code;

	@Column
	private String description;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
