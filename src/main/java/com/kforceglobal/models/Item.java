package com.kforceglobal.models;

import java.math.BigDecimal;

import javax.persistence.*;

@Entity
@Table(name = "item")
public class Item extends Identifiable {

	@Column(nullable = false, unique = true)
	private String code;
	
	@Column
	private String description;
	
	@Column
	private BigDecimal price;
	
	@JoinColumn(name = "item_category_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private ItemCategory category;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public ItemCategory getCategory() {
		return category;
	}

	public void setCategory(ItemCategory category) {
		this.category = category;
	}
	
}
