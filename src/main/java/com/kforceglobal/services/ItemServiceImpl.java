package com.kforceglobal.services;

import java.util.List;

import javax.persistence.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kforceglobal.models.Item;
import com.kforceglobal.repositories.ItemRepository;

@Service
public class ItemServiceImpl implements ItemService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ItemServiceImpl.class);

	@Autowired
	private ItemRepository itemRepository;

	@PersistenceContext
	private EntityManager em;

	/**
	 * Dependency injected.
	 * 
	 * @param itemRepository
	 *            {@link ItemServiceImpl}
	 */
	public void setItemRepository(ItemRepository itemRepository) {
		this.itemRepository = itemRepository;
	}

	@Override
	public List<Item> findAll() {
		return itemRepository.findAll();
	}

	@Override
	public Item findById(int id) {
		return itemRepository.getOne(id);
	}

	@Override
	public Item findByCode(String code) {
		return itemRepository.findByCode(code);
	}

	@Override
	public Item findByDescription(String description) {
		return itemRepository.findByDescription(description);
	}

	@Override
	public boolean create(Item clazz) {
		boolean result = false;
		try {
			itemRepository.saveAndFlush(clazz);
			result = true;
		} catch (Exception e) {
			LOGGER.error("Cause: {}", e.toString(), e);
		}
		return result;
	}

	@Override
	public boolean update(Item clazz) {
		boolean result = false;
		try {
			itemRepository.saveAndFlush(clazz);
			result = true;
		} catch (Exception e) {
			LOGGER.error("Cause: {}", e.toString(), e);
		}
		return result;
	}

	@Override
	public boolean deleteById(int id) {
		boolean result = false;
		try {
			em.createQuery("DELETE FROM " + Item.class.getSimpleName() + " i WHERE i.id = :id").setParameter("id", id)
					.executeUpdate();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Cause: {}", e.toString(), e);
		}
		return result;
	}

	@Transactional
	@Override
	public boolean deleteByCode(String code) {
		boolean result = false;
		try {
			em.createQuery("DELETE FROM " + Item.class.getSimpleName() + " i WHERE i.code = :code")
					.setParameter("code", code).executeUpdate();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Cause: {}", e.toString(), e);
		}
		return result;
	}

}
