package com.kforceglobal.services;

import java.util.List;

/**
 * Contains common transaction.
 * 
 * @author elagera
 *
 * @param <T>
 *            Accessor class
 */
public interface BaseDao<T> {

	public List<T> findAll();

	public T findById(int id);

	public T findByDescription(String description);

	public T findByCode(String code);

	public boolean create(T clazz);

	public boolean update(T clazz);

	public boolean deleteById(int id);

	public boolean deleteByCode(String code);

}
