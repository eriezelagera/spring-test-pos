package com.kforceglobal.services;

import com.kforceglobal.models.Item;

/**
 * An adapter class of {@link BaseDao} for {@link Item}.
 * 
 * @author elagera
 *
 */
public interface ItemService extends BaseDao<Item> {

}
