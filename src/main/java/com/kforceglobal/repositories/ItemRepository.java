package com.kforceglobal.repositories;

import org.springframework.stereotype.Repository;
import com.kforceglobal.models.Item;

@Repository
public interface ItemRepository extends BaseRepository<Item, Integer> {

	public Item findByCode(String code);
	
}
