package com.kforceglobal.repositories;

import org.springframework.stereotype.Repository;

import com.kforceglobal.models.ItemCategory;

@Repository
public interface ItemCategoryRepository extends BaseRepository<ItemCategory, Integer> {

	
}
