package com.kforceglobal.repositories;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * Base repository.
 * 
 * @author elagera
 *
 * @param <T> Accessor class
 */
@NoRepositoryBean
public interface BaseRepository<T, ID extends Serializable> extends JpaRepository<T, ID> {

	public T findByDescription(String description);
	
	public void deleteById(int id);

	public void deleteByCode(String code);
	
}
