package com.kforceglobal;

import java.io.*;
import java.math.*;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.concurrent.Executors;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.kforceglobal.models.Item;
import com.kforceglobal.services.ItemServiceImpl;

@SpringBootApplication
public class SpringTestPosApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpringTestPosApplication.class);
	/**
	 * Global console input.
	 */
	private static final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	/**
	 * Force Locale to Philippines.
	 */
	private static final NumberFormat PH_FORMAT = NumberFormat.getCurrencyInstance(Locale.forLanguageTag("en-PH"));

	@Autowired
	private ItemServiceImpl itemService;

	@Bean
	public CommandLineRunner exec() {
		return new CommandLineRunner() {

			@Override
			public void run(String... args) throws Exception {
				Executors.newSingleThreadExecutor().submit(() -> {
					try {
						String in = "";
						do {
							mainMenu();
							in = br.readLine();
							switch (in.toLowerCase()) {
							case "a":
								blankSpace(10);
								System.out.println("Adding new item");
								System.out.println();
								final Item item = new Item();
								final String cname = getInput("name");
								item.setDescription(cname);
								item.setCode(getInput("code"));
								item.setPrice(new BigDecimal(getInput("price")).setScale(2, RoundingMode.HALF_UP));

								if (itemService.create(item)) {
									System.out.println("Item added!");
								} else {
									System.out.println(String.format("Cannot add %s!", cname));
								}
								br.readLine();
								break;
							case "u":
								blankSpace(10);
								System.out.println("Updating item detail");
								System.out.println();
								final String ucode = getInput("code");
								// Retrieve existing item based on
								// specified description
								final Item anItem = itemService.findByCode(ucode);
								if (anItem == null) { // Probably not found
									System.out.println(String.format("Item code %s not found", ucode));
								} else {
									printItemDetail(anItem);

									// Get new details
									final String uname = anItem.getDescription();
									final BigDecimal uprice = anItem.getPrice();
									String newName = getInput(String.format("name [%s]", uname));
									if (newName.trim().isEmpty()) {
										newName = uname;
									}
									String newCode = getInput(String.format("code [%s]", ucode));
									if (newCode.trim().isEmpty()) {
										newCode = ucode;
									}
									BigDecimal newPrice = new BigDecimal(getInput(
											String.format("price [%s]", PH_FORMAT.format(uprice.doubleValue()))));
									// Update item
									anItem.setDescription(newName);
									anItem.setCode(newCode);
									anItem.setPrice(newPrice);
									if (itemService.update(anItem)) {
										System.out.println(String.format("%s updated successfully!", newName));
									} else {
										System.out.println(String.format("Cannot update %s", uname));
									}
								}
								br.readLine();
								break;
							case "d":
								blankSpace(10);
								System.out.println("Delete an existing item");
								System.out.println();
								final String dcode = getInput("code");

								if (itemService.deleteByCode(dcode)) {
									System.out.println(String.format("%s successfully deleted!", dcode));
								} else {
									System.out.println(String.format("Cannot delete item with code %s!", dcode));
								}
								br.readLine();
								break;
							case "l":
								blankSpace(10);
								for (Item exItem : itemService.findAll()) {
									printItemDetail(exItem);
									br.readLine();
								}
								br.readLine();
								break;
							default:
								break;
							}
						} while (!in.startsWith("e"));
						System.out.println("Application will now exit...");
						System.exit(0);
					} catch (IOException e) {
						LOGGER.error("Cause: {}", e.toString(), e);
					}
				});
			}
		};
	}

	private static int getInputInt(String description) throws IOException {
		int result = -999;
		do {
			try {
				result = Integer.parseInt(getInput(description));
			} catch (NumberFormatException e) {
				System.out.println();
				System.out.println(String.format("Invalid item %s!", description));
				System.out.println();
			}
		} while (result == -999);
		return result;
	}

	/**
	 * Get user input.
	 * 
	 * @param description
	 *            A description of the input
	 * @return User input
	 * @throws IOException
	 *             Thrown by {@link BufferedReader#readLine()}
	 */
	private static String getInput(String description) throws IOException {
		String result = "";
		boolean invalid = true;
		do {
			System.out.print(String.format("Enter item %s: ", description));
			result = br.readLine();

			invalid = (result == null);
			if (invalid) {
				System.out.println();
				System.out.println(String.format("Invalid item %s!", description));
				System.out.println();
			}
		} while (invalid);
		return result;
	}

	private static void printItemDetail(Item item) {
		if (item != null) {
			System.out.println();
			System.out.println("   Item Details");
			System.out.println(String.format("Name: %s", item.getDescription()));
			System.out.println(String.format("Code: %s", item.getCode()));
			System.out.println(String.format("Price: %s", PH_FORMAT.format(item.getPrice().doubleValue())));
			System.out.println();
		}
	}

	/**
	 * Print row of blank spaces.
	 * 
	 * @param row
	 *            Space row count
	 */
	private static void blankSpace(int row) {
		for (int i = 0; i < row; i++) {
			System.out.println();
		}
	}

	/**
	 * Print main menu.
	 */
	private static void mainMenu() {
		blankSpace(10);
		System.out.println("   My Canteen Inventory");
		System.out.println();
		System.out.println("  [A]dd item");
		System.out.println("  [U]pdate item");
		System.out.println("  [D]elete item");
		System.out.println("  [L]ist of items");
		System.out.println("  [E]xit");
		System.out.println();
		System.out.print("Enter option: ");
	}

	public static void main(String[] args) {
		PropertyConfigurator.configure("log4j.properties");
		SpringApplication.run(SpringTestPosApplication.class, args);
	}

}
